<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>編集</title>
    </head>
    <div class="main-contents">
            <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="message">
                            <li><c:out value="${message}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session" />
            </c:if>

            <form action="settings" method="post">

				<input type="hidden" value="${edit.id}" name="id" />
                <label for="account">ログインID（半角英数字6文字以上20文字以下）</label> <br />
                <input name="account"id="account" value="${edit.account}" /> <br />

                <label for="password">パスワード</label> <br />
                <input name="password" type="password" id="password" /> <br />

                <label for="password2">パスワード（確認用）</label> <br />
                <input name="password2" type="password" id="password2" /> <br />

                <label for="name">ユーザー名</label> <br />
                <input name="name" id="name" value="${edit.name}" /> <br />

                <label for="branch">支店</label> <br />
                <select name="branch"  id="branch" >
               		<c:forEach items="${branch}" var="br">
		                <c:if test="${br.id == edit.branch}">
						 <option value="${br.id}" selected>${br.branch_name}</option>
						</c:if>
						<c:if test="${br.id != edit.branch}">
						 <option value="${br.id}">${br.branch_name}</option>
						</c:if>
                    </c:forEach>
                </select><br />

                <label for="department_position">部署・役職</label> <br />
                <select name="department_position"  id="department_position">
                	<c:forEach items="${department_position}" var="dp">
                		<c:if test="${dp.id == edit.department_position}">
						 <option value="${dp.id}" selected>${dp.department_position_name}</option>
						</c:if>
						<c:if test="${dp.id != edit.department_position}">
						 <option value="${dp.id}">${dp.department_position_name}</option>
						</c:if>
                    </c:forEach>
                </select><br />

                <input type="submit" value="更新" /> <br /> <a href="./">戻る</a>
            </form>
        </div>
</html>