<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>掲示板システム</title>
        <style>
			.table1 {
			  border: 1px solid gray;
			}
			.table1 th, .table1 td {
			  border: 1px solid gray;
			}
		</style>
    </head>

    <body>

        <div class="main-contents">
            <div class="header">
                <a href="signup">新規登録</a>
            </div>
        </div>

    	<div class="registration-info">
	    	<table class="table1">
				<tr><th>ログインID</th><th>名前</th><th>支店名</th><th>部署・役職</th></tr>
				<c:forEach items="${info}" var="in">
					<tr>
						<td><c:out value="${in.account}" /></td>
						<td><c:out value="${in.name}" /></td>
						<td><c:out value="${in.branch_name}" /></td>
						<td><c:out value="${in.department_position_name} "/></td>
						<td><a href="settings?Id=${in.id}" >編集</a></td>
					</tr>
				</c:forEach>
			</table>
		</div>
	</body>
</html>

