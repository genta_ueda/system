package ueda_genta.dao;

import static ueda_genta.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;

import ueda_genta.beans.Department_position;
import ueda_genta.exception.SQLRuntimeException;

@WebServlet("/Department_positionDao")
public class Department_positionDao extends HttpServlet {

	public List<Department_position> select(Connection connection) {

        PreparedStatement ps = null;

        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("department_positions.id as id, ");
            sql.append("department_positions.department_position_name as department_position_name ");
            sql.append("FROM department_positions ");

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<Department_position> ret = toDepartment_positionList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

	private List<Department_position> toDepartment_positionList(ResultSet rs)
            throws SQLException {

        List<Department_position> ret = new ArrayList<Department_position>();
        try {
            while (rs.next()) {
                String department_positionName = rs.getString("department_position_name");
                int id = rs.getInt("id");

                Department_position department_position = new Department_position();
                department_position.setDepartment_position_name(department_positionName);
                department_position.setId(id);

                ret.add(department_position);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

}
