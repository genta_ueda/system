package ueda_genta.dao;

import static ueda_genta.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import ueda_genta.beans.User;
import ueda_genta.beans.UserInfo;
import ueda_genta.exception.NoRowsUpdatedRuntimeException;
import ueda_genta.exception.SQLRuntimeException;

public class UserDao {

    public void insert(Connection connection, User user) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO users ( ");
            sql.append("account");
            sql.append(", password");
            sql.append(", name");
            sql.append(", branch");
            sql.append(", department_position");
            sql.append(", created_date");
            sql.append(", updated_date");
            sql.append(") VALUES (");
            sql.append("?"); // account
            sql.append(", ?"); // password
            sql.append(", ?"); // name
            sql.append(", ?"); // branch
            sql.append(", ?"); // department_position
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(", CURRENT_TIMESTAMP"); // updated_date
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, user.getAccount());
            ps.setString(2, user.getPassword());
            ps.setString(3, user.getName());
            ps.setInt(4, user.getBranch());
            ps.setInt(5, user.getDepartment_position());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
    private List<User> toUserList(ResultSet rs) throws SQLException {

        List<User> ret = new ArrayList<User>();
        try {
            while (rs.next()) {
                int id = rs.getInt("id");
                String account = rs.getString("account");
                String password = rs.getString("password");
                String name = rs.getString("name");
                int branch = rs.getInt("branch");
                int department_position = rs.getInt("department_position");
                Timestamp createdDate = rs.getTimestamp("created_date");
                Timestamp updatedDate = rs.getTimestamp("updated_date");


                User user = new User();
                user.setId(id);
                user.setAccount(account);
                user.setPassword(password);
                user.setName(name);
                user.setBranch(branch);
                user.setDepartment_position(department_position);
                user.setCreatedDate(createdDate);
                user.setUpdatedDate(updatedDate);

                ret.add(user);
            }
            return ret;
        } finally {
            close(rs);
        }
    }
    private List<UserInfo> toEditUserList(ResultSet rs) throws SQLException {

        List<UserInfo> ret = new ArrayList<UserInfo>();
        try {
            while (rs.next()) {
                int id = rs.getInt("id");
                String account = rs.getString("account");
                String password = rs.getString("password");
                String name = rs.getString("name");
                int branch = rs.getInt("branch");
                int department_position = rs.getInt("department_position");
                Timestamp createdDate = rs.getTimestamp("created_date");
                Timestamp updatedDate = rs.getTimestamp("updated_date");


                UserInfo user = new UserInfo();
                user.setId(id);
                user.setAccount(account);
                user.setPassword(password);
                user.setName(name);
                user.setBranch(branch);
                user.setDepartment_position(department_position);
                user.setCreatedDate(createdDate);
                user.setUpdatedDate(updatedDate);

                ret.add(user);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

    public UserInfo getUser(Connection connection, int id) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM users WHERE id = ?";



            ps = connection.prepareStatement(sql);
            ps.setInt(1, id);

            ResultSet rs = ps.executeQuery();
            List<UserInfo> userList = toEditUserList(rs);
            if (userList.isEmpty() == true) {
                return null;
            } else if (2 <= userList.size()) {
                throw new IllegalStateException("2 <= userList.size()");
            } else {
                return userList.get(0);
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
    public void update(Connection connection, User user) {

 		PreparedStatement ps = null;
 		try {
 			StringBuilder sql = new StringBuilder();
 			sql.append("UPDATE users SET");
 			sql.append("  account = ?");
 			sql.append(", password = ?");
 			sql.append(", name = ?");
 			sql.append(", branch = ?");
 			sql.append(", department_position = ?");
 			sql.append(", updated_date = CURRENT_TIMESTAMP");
 			sql.append(" WHERE");
 			sql.append(" id = ?");

 			ps = connection.prepareStatement(sql.toString());

 			ps.setString(1, user.getAccount());
 			ps.setString(2, user.getPassword());
 			ps.setString(3, user.getName());
 			ps.setInt(4, user.getBranch());
 			ps.setInt(5, user.getDepartment_position());
 			ps.setInt(6, user.getId());

 			int count = ps.executeUpdate();
 			if (count == 0) {
 				throw new NoRowsUpdatedRuntimeException();
 			}
 		} catch (SQLException e) {
 			throw new SQLRuntimeException(e);
 		} finally {
 			close(ps);
 		}

 	}

 }


