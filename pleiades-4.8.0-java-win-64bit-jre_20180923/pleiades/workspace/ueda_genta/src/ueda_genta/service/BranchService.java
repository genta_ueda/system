package ueda_genta.service;

import static ueda_genta.utils.CloseableUtil.*;
import static ueda_genta.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import javax.servlet.http.HttpServlet;

import ueda_genta.beans.Branch;
import ueda_genta.dao.BranchDao;

public class BranchService extends HttpServlet {

    public List<Branch> getBranch() {

        Connection connection = null;
        try {
            connection = getConnection();

            BranchDao branchDao = new BranchDao();
            List<Branch> ret = branchDao.select(connection);

            commit(connection);

            return ret;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }


}
